﻿using Npgsql;
using System;

namespace DataMigration
{
    class Database
    {
        private string host;
        private string Host
        {
            get;
            set;
        }

        private string user;
        private string User
        {
            get;
            set;
        }

        private string dbName;
        private string DbName
        {
            get;
            set;
        }

        private string password;
        private string Password
        {
            get;
            set;
        }

        private string port;
        private string Port
        {
            get;
            set;
        }

        private string sslMode;
        private string SslMode
        {
            get;
            set;
        }

        public Database(string host, string user, string dbName, string password, string port, string sslMode)
        {
            Host = host;
            User = user;
            DbName = dbName;
            Password = password;
            Port = port;
            SslMode = sslMode;
        }

        
        public string getConnString()
        {
            return String.Format(
                    "Server={0}; User Id={1}; Database={2}; Port={3}; Password={4};SSLMode={5};",
                    Host,
                    User,
                    DbName,
                    Port,
                    Password,
                    SslMode);
        }

        public void connectDB()
        {
            NpgsqlConnection npgsqlConnection = new NpgsqlConnection(this.getConnString());
        }
    }
}
