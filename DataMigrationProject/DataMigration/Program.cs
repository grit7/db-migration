﻿using Npgsql;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;

namespace DataMigration
{
    class Program
    {
        static void Main(string[] args)
        {
            Database internelDB = new Database("localhost", "postgres", "dvdrental", "5588", "5432", "Prefer");
            Database externelDB = new Database("101.101.216.250", "postgres", "postgres", "csm55888", "5432", "Disable");
            List<string> tablesByInternelDB = new List<string>();
            
            Console.WriteLine(internelDB.getConnString());
            Console.WriteLine(externelDB.getConnString());

            using (var internelConn = new NpgsqlConnection(internelDB.getConnString()))
            using (var externelConn = new NpgsqlConnection(externelDB.getConnString() + "Trust Server Certificate = true"))
            {
                Console.Out.WriteLine("Opening connection");
                internelConn.Open();
                externelConn.Open();

                using (var internelCommand = new NpgsqlCommand("SELECT VERSION();", internelConn))
                using (var externelCommand = new NpgsqlCommand("SELECT VERSION();", externelConn))
                {

                    var internelReader = internelCommand.ExecuteReader();
                    var externelReader = externelCommand.ExecuteReader();
                    while (internelReader.Read() && externelReader.Read())
                    {
                        Console.WriteLine(
                            string.Format(
                                "Reading version=\n(internel : {0},\n externel : {1})",
                                internelReader.GetString(0).ToString(),
                                externelReader.GetString(0).ToString()
                                )
                            );
                    }
                    internelReader.Close();
                    externelReader.Close();
                }

                using (var internelCommand = new NpgsqlCommand("SELECT * FROM information_schema.tables WHERE table_type = 'BASE TABLE' and table_schema = 'public';", internelConn))
                {
                    var internelReader = internelCommand.ExecuteReader();
                    while (internelReader.Read())
                    {
                        tablesByInternelDB.Add(internelReader.GetString(2));
                    }
                    internelReader.Close();
                }

                foreach (string table in tablesByInternelDB)
                {
                    Console.WriteLine(table);
                }

                Debug.Assert(tablesByInternelDB.Count > 0);
                foreach (string table in tablesByInternelDB)
                {
                    using (var internelCommand = new NpgsqlCommand($"SELECT * FROM {table};", internelConn))
                    {
                        var internelReader = internelCommand.ExecuteReader();
                        while (internelReader.Read())
                        {
                            /*Debug.Assert(preprocessing completed)*/
                            using (var externelCommand = new NpgsqlCommand($"INSERT INTO actor (actor_id, first_name, last_name, last_update) VALUES (@actor_id, @first_name, @last_name, @last_update)", externelConn))
                            {
                                externelCommand.Parameters.AddWithValue("actor_id", internelReader.GetInt32(0));
                                externelCommand.Parameters.AddWithValue("first_name", internelReader.GetString(1));
                                externelCommand.Parameters.AddWithValue("last_name", internelReader.GetString(2));
                                externelCommand.Parameters.AddWithValue("last_update", internelReader.GetTimeStamp(3));
                                externelCommand.ExecuteNonQuery();
                            }
                        }
                        internelReader.Close();
                    }
                    break;
                }
            }
        }
    }
}
