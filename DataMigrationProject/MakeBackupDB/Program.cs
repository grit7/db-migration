﻿using Npgsql;
using System;

namespace MakeBackupDB
{
    class Program
    {
        static void Main(string[] args)   
        {
            string connStr = "Server=localhost;Port=5432;User Id=postgres;Password=5588;";

            using (var internelConn = new NpgsqlConnection(connStr))
            {
                Console.Out.WriteLine("Opening connection");
                internelConn.Open();

                using (var internelCommand = new NpgsqlCommand("ALTER DATABASE backup_dvdrental RENAME TO dvdrental;", internelConn))
                {

                    var internelReader = internelCommand.ExecuteReader();
                    internelReader.Close();
                }
                /*ktl@123
                using (var internelCommand = new NpgsqlCommand(@"
                        CREATE DATABASE backup_dvdrental
                        WITH OWNER = postgres
                        ENCODING = 'UTF8'
                        LC_COLLATE = 'Korean_Korea.949'
                        LC_CTYPE = 'Korean_Korea.949'
                        TABLESPACE = pg_default
                        CONNECTION LIMIT = -1;
                        ", internelConn))
                {
                    var internelReader = internelCommand.ExecuteReader();
                    internelReader.Close();
                }
                */
            }
        }
    }
}
