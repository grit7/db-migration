﻿using System;

using System.IO;
using System.Data;
using HtmlAgilityPack;

namespace Preprocessing
{
    class Program
    {
        static string RtfToHtml(string path)
        {
            SautinSoft.RtfToHtml r = new SautinSoft.RtfToHtml();
            string rtfString = File.ReadAllText(path);

            r.ImageStyle.IncludeImageInHtml = true;

            string htmlString = r.ConvertString(rtfString);

            return htmlString;
        }

        static void HtmlToCsv(string html, string path)
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);
            HtmlNodeCollection tableNode1 = htmlDoc.DocumentNode.SelectNodes("//table");
            Console.WriteLine(tableNode1.Count);

            HtmlNode tableNode = htmlDoc.DocumentNode.SelectSingleNode("//table");
            HtmlNodeCollection trNodes = tableNode.SelectNodes("tr");

            DataTable dataTable = new DataTable();
            DataRow row = null;

            for (int i = 0; i < trNodes.Count; i++)
            {
                HtmlNodeCollection tdNodes = trNodes[i].SelectNodes("td");
                if (i == 0)
                {
                    for (int j = 0; j < tdNodes.Count; j++)
                    {
                        string columnName = tdNodes[j].InnerText;
                        columnName = columnName.Replace('\n', ' ');
                        columnName = columnName.Replace('\r', ' ');
                        columnName = columnName.Replace("&mu;", "m");
                        dataTable.Columns.Add(new DataColumn(columnName, typeof(string)));
                    }
                }
                else
                {
                    row = dataTable.NewRow();
                    for (int j = 0; j < tdNodes.Count; j++)
                    {
                        row[j] = tdNodes[j].InnerText;
                    }
                    dataTable.Rows.Add(row);
                }
            }

            for (int i = 0; i < dataTable.Rows.Count; i++)
            {
                for (int j = 0; j < dataTable.Columns.Count; j++)
                {
                    Console.WriteLine(dataTable.Rows[i][j].ToString());
                }
                Console.WriteLine("=====");
            }

            ExportToCSV(dataTable, path);
        }


        static void ExportToCSV(DataTable dtDataTable, string strFilePath)
        {

            StreamWriter sw = new StreamWriter(strFilePath, false, System.Text.Encoding.Default);
            //headers   
            for (int i = 0; i < dtDataTable.Columns.Count; i++)
            {
                sw.Write(dtDataTable.Columns[i].ToString().Trim());
                if (i < dtDataTable.Columns.Count - 1)
                {
                    sw.Write(",");
                }
            }
            sw.Write(sw.NewLine);
            foreach (DataRow dr in dtDataTable.Rows)
            {
                for (int i = 0; i < dtDataTable.Columns.Count; i++)
                {
                    if (!Convert.IsDBNull(dr[i]))
                    {
                        string value = dr[i].ToString().Trim();
                        if (value.Contains(','))
                        {
                            value = String.Format("\"{0}\"", value);
                            sw.Write(value);
                        }
                        else
                        {
                            sw.Write(dr[i].ToString().Trim());
                        }
                    }
                    if (i < dtDataTable.Columns.Count - 1)
                    {
                        sw.Write(",");
                    }
                }
                sw.Write(sw.NewLine);
            }
            sw.Close();
        }

        static void Main(string[] args)
        {
            string path = @"C:\Users\Seongmin\Documents\ktl\";
            string html = Program.RtfToHtml(path + "19-2108178.rtf");

            HtmlToCsv(html, path + "OneTableFile.csv");
        }
    }
}