﻿using Npgsql;
using System;

namespace DataMigrationMiniProject
{
    class Program
    {

        static void Main(string[] args)
        {
            insertTest();

            Console.WriteLine("Press RETURN to exit");
            Console.ReadLine();
        }

        static void insertTest()
        {
            Database externelDB = new Database("101.101.216.250", "postgres", "postgres", "csm55888", "5432", "Disable");

            using (var externelConn = new NpgsqlConnection(externelDB.getConnString() + "Trust Server Certificate = true"))
            {
                Console.Out.WriteLine("Opening connection");
                externelConn.Open();

                using (var externelCommand = new NpgsqlCommand("DROP TABLE IF EXISTS  actor", externelConn))
                {
                    externelCommand.ExecuteNonQuery();
                    Console.Out.WriteLine("Finished dropping table(if existed)");
                }

                using (var externelCommand = new NpgsqlCommand("CREATE TABLE actor(actor_id integer NOT NULL, first_name character varying(45), last_name character varying(45), last_update timestamp without time zone NOT NULL DEFAULT now(), PRIMARY KEY (actor_id))", externelConn))
                {
                    externelCommand.ExecuteNonQuery();
                    Console.Out.WriteLine("Finished creating table");
                }
            }
        }
    }
}
