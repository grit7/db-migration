﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Text;

namespace DataMigrationMiniProject
{
    class Database
    {
        private string host;
        private string Host
        {
            get { return host; }
            set
            {
                if (String.IsNullOrEmpty(value) == false)
                {
                    host = value;
                }
            }
        }

        private string user;
        private string User
        {
            get { return user; }
            set
            {
                if (String.IsNullOrEmpty(value) == false)
                {
                    user = value;
                }
            }
        }

        private string dbName;
        private string DbName
        {
            get { return dbName; }
            set
            {
                if (String.IsNullOrEmpty(value) == false)
                {
                    dbName = value;
                }
            }
        }

        private string password;
        private string Password
        {
            get { return password; }
            set
            {
                if (String.IsNullOrEmpty(value) == false)
                {
                    password = value;
                }
            }
        }

        private string port;
        private string Port
        {
            get { return port; }
            set
            {
                if (String.IsNullOrEmpty(value) == false)
                {
                    port = value;
                }
            }
        }

        private string sslMode;
        private string SslMode
        {
            get { return sslMode; }
            set
            {
                if (String.IsNullOrEmpty(value) == false)
                {
                    sslMode = value;
                }
            }
        }

        public Database(string host, string user, string dbName, string password, string port, string sslMode)
        {
            Host = host;
            User = user;
            DbName = dbName;
            Password = password;
            Port = port;
            SslMode = sslMode;
        }

        
        public string getConnString()
        {
            return String.Format(
                    "Server={0}; User Id={1}; Database={2}; Port={3}; Password={4};SSLMode={5};",
                    Host,
                    User,
                    DbName,
                    Port,
                    Password,
                    SslMode);
        }

        public void connectDB()
        {
            NpgsqlConnection npgsqlConnection = new NpgsqlConnection(this.getConnString());
            
        }
    }
}
